package com.baselogic.pid.enums;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by mickknutson on 8/25/16.
 */
public class EnumsTest {

    @Test(expected = RuntimeException.class)
    public void test_throw_new_RuntimeException(){

//        int a = 0;
//        int b = 0;
//        int chuckNorris = a / b;

        throw new RuntimeException("Only Chuck Norris can divide by Zero!!!");

    }

    @Test
    public void test__activities(){
        Activity activity = Activity.BASE;
        assertThat(activity.name(), is("BASE"));
        assertThat(Activity.valueOf("BASE"), is(activity));
        assertThat(Activity.values().length, is(3));
    }

    @Test
    public void test__environments(){
        Environments environments = Environments.DEV;
        assertThat(environments.name(), is("DEV"));
        assertThat(Environments.valueOf("DEV"), is(environments));
        assertThat(Environments.values().length, is(3));
    }

    @Test
    public void test__objectType(){
        ObjectType objectType = ObjectType.EARTH;
        assertThat(objectType.name(), is("EARTH"));
        assertThat(ObjectType.valueOf("EARTH"), is(objectType));
        assertThat(ObjectType.values().length, is(5));

        ObjectType span = ObjectType.instance("SPAN");
        assertThat(span, is(ObjectType.SPAN));

        ObjectType invalid = ObjectType.instance("FOOBAR");
        assertThat(invalid, is(nullValue()));
    }

    @Test
    public void test__roles(){
        Roles roles = Roles.ADMIN;
        assertThat(roles.name(), is("ADMIN"));
        assertThat(Roles.valueOf("ADMIN"), is(roles));
        assertThat(Roles.values().length, is(3));
    }

} // The End...
