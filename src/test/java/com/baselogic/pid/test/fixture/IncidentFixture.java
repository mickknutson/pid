package com.baselogic.pid.test.fixture;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.domain.incident.*;
import com.baselogic.pid.enums.Gender;
import com.baselogic.pid.enums.ObjectType;
import com.baselogic.pid.repository.IncidentRepository;
import com.baselogic.pid.repository.LocationRepository;
import com.baselogic.pid.repository.UserRepository;
import com.baselogic.pid.service.PersonService;
import com.baselogic.pid.service.UserService;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.web.servlet.RequestBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;



import java.net.URL;
import java.util.*;

/**
 * Incident Request Fixture
 */
@Data
@Slf4j
public class IncidentFixture {
/*

    @SneakyThrows
    public static Incident incident(int i) {

        Incident incident = new Incident("NameOfJumper" + i);
//        incident.setName("NameOfJumper" + i);
        incident.setPerson("Person"+ i);
        incident.setDescription("Foo description "+ i);

        // FIXME: Need to ensure this is saved 1st:
//        incident.setLocation(HIGH_NOSE);
//        initLocation();

        incident.setActivity("BASE"); // Activity.BASE
        incident.setTagLine("tagline-"+ i);
        incident.setReferenceUrls(new ArrayList<URL>(){{add(new URL("http", "blincmagazine.com", 80, "/"));}});

        return incident;
    }
*/

    //-----------------------------------------------------------------------//
/*

    public static List<Incident> incidents(int size) {
        List<Incident> incidents = new ArrayList<>(size);
        for(int i = 0; i < size; i++){
            incidents.add(incident(i));
        }
        return incidents;
    }

    public static void initIncident(IncidentRepository incidentRepository, int numberOfIncidents){
        for(int i = 0; i < numberOfIncidents; i++) {

            Incident incident = IncidentFixture.incident(i);
//            incident.setLocation(HIGH_NOSE);
            List<Comment> comments = IncidentFixture.comments(incident.getTagLine());

            comments.forEach((comment) -> {
//                Mono<Comment> c = commentRepository.insert(comment);
//                incident.addComment(c.block());
                incident.addComment(comment);
            });

            log.info("incident to be saved: {}", incident);
            incidentRepository.save(incident);

            log.info("incident saved: {}", incident);
            log.info("incident id: {}", incident.getId());

        }
    }
*/





    //-----------------------------------------------------------------------//
    public static Comment comment(int i, String tag, Class clazz) {
        Comment comment = new Comment();
        comment.setName(tag +"-comment-"+i);
        comment.setComment("rome random dev comment - " +i);
        comment.setOwningClass(clazz.getSimpleName());
        return comment;
    }

    public static List<Comment> comments(String tag) {
        List<Comment> comments = new ArrayList<>();
        comments.add(comment(1, tag, Incident.class));
        comments.add(comment(2, tag, Incident.class));
        comments.add(comment(3, tag, Incident.class));
        return comments;
    }


    //-----------------------------------------------------------------------//
    public static User user(int i, String tag) {
        User user = new User();
        user.setUsername(tag + i);
        user.setPassword(tag + i);
        user.setId(tag + i);
        user.addRole(tag + i);
        return user;
    }

    public static void initUsers(UserRepository userRepository, int numberOfUsers){
        for(int i = 0; i < numberOfUsers; i++) {
            userRepository.save(user(i, "demo"));
        }
    }






    //-----------------------------------------------------------------------//
    /*private static Person person(int i, String tag) {
        Person person = new Person();
        person.setName(tag + i);
        person.setAge(i);
        person.setId(tag + i);
        person.setGender(Gender.MALE);
        return person;
    }*/






    //-----------------------------------------------------------------------//
    private RequestBuilder newUserToPost() {
        RequestBuilder req = post("/incidents")
                .param("email", "bob1@example.com")
                .param("firstName", "Bob")
                .param("lastName", "One")
                .param("password", "bob1")
                .accept(MediaType.TEXT_HTML)
//                .with(csrf())
                ;

        return req;
    }

    private RequestBuilder existingUserToPost() {
        RequestBuilder req = post("/incidents")
                .param("email", "user1@example.com")
                .param("firstName", "User")
                .param("lastName", "One")
                .param("password", "user")
                .accept(MediaType.TEXT_HTML)
//                .with(csrf())
                ;

        return req;
    }


    //-----------------------------------------------------------------------//
    //-----------------------------------------------------------------------//
}
