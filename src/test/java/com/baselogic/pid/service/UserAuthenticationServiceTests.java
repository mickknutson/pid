package com.baselogic.pid.service;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.repository.UserRepository;
import com.baselogic.pid.test.fixture.IncidentFixture;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.util.ReflectionTestUtils;
import reactor.core.publisher.Mono;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;


//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class UserAuthenticationServiceTests {

    @Autowired
    @InjectMocks // target: class under test
    UserAuthenticationService service;

    @Mock
    UserRepository mock;


    //--- Test methods ----------------------------------------------------//

    @Before
    public void beforeEachTest(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test__findByUsername() {
//        String tag = "testUser";
//        User user = IncidentFixture.user(123, tag);
//
//        Mono<User> mono = Mono.just(user);
//
//        // Expectations
//        when(mock.findByUsername(tag)).thenReturn(mono);
//
//        ReflectionTestUtils.setField(service, "repository", mock);
//
//        UserDetails result = service.loadUserByUsername(tag);
//
//        assertThat(result, is(notNullValue()));
//        assertThat(result.getUsername(), is(tag + 123));
//        assertThat(result.getPassword(), is(tag + 123));
//        assertThat(result.getAuthorities().size(), is(1));
//
//        assertThat(result.isAccountNonExpired(), is(true));
//        assertThat(result.isAccountNonLocked(), is(true));
//        assertThat(result.isCredentialsNonExpired(), is(true));
//        assertThat(result.isEnabled(), is(true));
    }

    @Test//(expected = UsernameNotFoundException.class)
    public void test__findByUsername_throws_UsernameNotFoundException() {
//        String tag = "testUser";
//
//        // Expectations
//        //noinspection unchecked
//        when(mock.findByUsername(tag)).thenThrow(UsernameNotFoundException.class);
//
//        ReflectionTestUtils.setField(service, "repository", mock);
//        service.loadUserByUsername(tag);
    }


//    @Test(expected = UsernameNotFoundException.class)
//    public void test__findByUsername_null_username_UsernameNotFoundException() {
//        String tag = "testUser";
//
//        // Expectations
//        when(mock.findByUsername(tag)).thenReturn(null);
//
//        ReflectionTestUtils.setField(service, "repository", mock);
//        service.loadUserByUsername(tag);
//    }

} // The End...
