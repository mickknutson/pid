package com.baselogic.pid.service;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.repository.IncidentRepository;
import com.baselogic.pid.test.fixture.IncidentFixture;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.util.ReflectionTestUtils;
import reactor.core.publisher.Mono;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;


//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class IncidentServiceTests {

//    @Autowired
//    @InjectMocks // target: class under test
    private IncidentService service;

//    @Mock
//    private IncidentRepository mock;


    //--- Test methods ----------------------------------------------------//

    @Before
    public void beforeEachTest(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void noop() {}

/*

//    @Test
    public void test__insert() {
        log.info("***START*************************************************");
        log.info("*** running...");
        String tag = "test__insert";
        Incident incident = IncidentFixture.incident(123);

        service.insert(incident);

        Incident incident2 = service.find("NameOfJumper123");
        log.info("incident2: {}", incident2);
    }
*/


//    @Test
//    public void test__insert() {
//        String tag = "test__insert";
//        Incident incident = IncidentFixture.incident(123);
//
//        Mono<User> mono = Mono.just(incident);
//
//        // Expectations
//        when(mock.findById(tag)).thenReturn(incident);
//
//        ReflectionTestUtils.setField(service, "repository", mock);
//
//        UserDetails result = service.loadUserByUsername(tag);
//
//        assertThat(result, is(notNullValue()));
//        assertThat(result.getUsername(), is(tag + 123));
//        assertThat(result.getPassword(), is(tag + 123));
//        assertThat(result.getAuthorities().size(), is(1));
//
//        assertThat(result.isAccountNonExpired(), is(true));
//        assertThat(result.isAccountNonLocked(), is(true));
//        assertThat(result.isCredentialsNonExpired(), is(true));
//        assertThat(result.isEnabled(), is(true));
//    }


} // The End...
