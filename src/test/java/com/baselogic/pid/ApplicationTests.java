package com.baselogic.pid;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.enums.Roles;
import com.baselogic.pid.repository.UserRepository;
import com.baselogic.pid.service.UserService;
import com.baselogic.pid.service.UserServiceImpl;
import com.baselogic.pid.test.fixture.IncidentFixture;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {

    @Autowired
    UserRepository userRepository;

//    @Autowired
//    UserService service;

    //--- Test methods ----------------------------------------------------//

    @Before
    public void beforeEachTest(){
        log.info("****************************************************");
        log.info("*** beforeEachTest...");
        userRepository.deleteAll();

//        IncidentFixture.initUsers(userRepository, 25);


        User user = new User("ApplicationTests_454", "mickknutson", "password");
        user.addRole(Roles.ADMIN.name());
        user.addRole(Roles.EDITOR.name());
        userRepository.save(user);

    }

    @After
    public void afterEachTest(){
        log.info("****************************************************");
        log.info("*** afterEachTest...");

        List<User> users = userRepository.findAll();
        users.forEach(System.out::println);
    }

    @Test
    public void noop(){
    }

} // The End...
