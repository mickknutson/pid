package com.baselogic.pid.controller;

import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.repository.IncidentRepository;
import com.baselogic.pid.test.fixture.IncidentFixture;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class IncidentEndpointTests {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private IncidentRepository incidentRepository;

    @Autowired
    private ReactiveMongoOperations operations;


    private final int numberOfIncidents = 5;


    @Before
    public void beforeEachTest(){
        log.info("****************************************************");
        log.info("*** beforeEachTest...");

        //        incidentRepository.deleteAll().then().block();
        operations.collectionExists(Incident.class)
                .flatMap(exists -> exists ? operations.dropCollection(Incident.class) : Mono.just(exists))
                .flatMap(o -> operations.createCollection(Incident.class, CollectionOptions.empty()))
                .then()
                .block();


//        IncidentFixture.initIncident(incidentRepository, numberOfIncidents);
        log.info("incidentRepository.count(): {}", incidentRepository.count());

        List<Incident> incidents = incidentRepository.findAll();

        incidents.forEach(System.out::println);

        log.info("----------------------------------------------------");

    }


    //---------------------------------------------------------------------------//

    @Test
    public void noop() {}


    /**
     * http://localhost:8080/incidents
     */
//    @Test
//    @WithUserDetails("user1")
    @WithMockUser(username = "user1", roles = "USER")
    public void test_GET_All_incident() {

        webTestClient.get().uri("/incidents")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Incident.class);
    }

    /**
     * http://localhost:8080/incidents
     */
/*
    @Test
    public void test_Create__POST_incident() {
        Incident object = IncidentFixture.incident(42);

        webTestClient.post().uri("/incidents")
                .body(BodyInserters.fromObject(object))
                .accept(MediaType.APPLICATION_JSON_UTF8)

                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Incident.class);
    }
*/

    /**
     * http://localhost:8080/incidents
     */
/*
//    @Test
    public void test_Update__PUT_incident() {
        //.content(objectMapper.writeValueAsBytes(new CreateClientRequest("Foo"))))
        //.perform(newUserToPost())

        Incident object = IncidentFixture.incident(43);

        webTestClient.put().uri("/incidents")
                .body(BodyInserters.fromObject(object))
                .accept(MediaType.APPLICATION_JSON_UTF8)

                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Incident.class);
    }
*/



    /**
     * http://localhost:8080/incidents
     */
//    @Test
//    public void test_DELETE_All_incident() {
//
//        webTestClient.delete().uri("/incidents/NameOfJumper1")
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .exchange()
//                .expectStatus().isUnauthorized()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBodyList(Incident.class);
//    }






    //---------------------------------------------------------------------------//


//    @Test
//    public void testCreateTweet() {
//        Tweet tweet = new Tweet("This is a Test Tweet");
//
//        webTestClient.post().uri("/tweets")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .body(Mono.just(tweet), Tweet.class)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBody()
//                .jsonPath("$.id").isNotEmpty()
//                .jsonPath("$.text").isEqualTo("This is a Test Tweet");
//    }

//    @Test
//    public void test_GetSingleIncident() {
//        Tweet tweet = tweetRepository.save(new Tweet("Hello, World!")).block();
//
//        webTestClient.get()
//                .uri("/tweets/{id}", Collections.singletonMap("id", tweet.getId()))
//                .exchange()
//                .expectStatus().isOk()
//                .expectBody()
//                .consumeWith(response ->
//                        Assertions.assertThat(response.getResponseBody()).isNotNull());
//    }

//    @Test
//    public void testGetSingleTweet() {
//        Tweet tweet = tweetRepository.save(new Tweet("Hello, World!")).block();
//
//        webTestClient.get()
//                .uri("/tweets/{id}", Collections.singletonMap("id", tweet.getId()))
//                .exchange()
//                .expectStatus().isOk()
//                .expectBody()
//                .consumeWith(response ->
//                        Assertions.assertThat(response.getResponseBody()).isNotNull());
//    }
//
//    @Test
//    public void testUpdateTweet() {
//        Tweet tweet = tweetRepository.save(new Tweet("Initial Tweet")).block();
//
//        Tweet newTweetData = new Tweet("Updated Tweet");
//
//        webTestClient.put()
//                .uri("/tweets/{id}", Collections.singletonMap("id", tweet.getId()))
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .body(Mono.just(newTweetData), Tweet.class)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBody()
//                .jsonPath("$.text").isEqualTo("Updated Tweet");
//    }
//
//    @Test
//    public void testDeleteTweet() {
//        Tweet tweet = tweetRepository.save(new Tweet("To be deleted")).block();
//
//        webTestClient.delete()
//                .uri("/tweets/{id}", Collections.singletonMap("id",  tweet.getId()))
//                .exchange()
//                .expectStatus().isOk();
//    }

}
