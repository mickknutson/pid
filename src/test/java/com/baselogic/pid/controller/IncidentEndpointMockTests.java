package com.baselogic.pid.controller;

import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.repository.IncidentRepository;
import com.baselogic.pid.test.fixture.IncidentFixture;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Mick Knutson
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(IncidentController.class)
@Slf4j
public class IncidentEndpointMockTests {

    // Reactive WebClient (SpringBootTest only)
//    @Autowired
//    private WebTestClient webTestClient;

    // HTML Unit:
//    @Autowired
//    private WebClient client;

    @Autowired
    private MockMvc mvc;


//    @MockBean
//    private UserDetailsService userDetailsService;

    @MockBean
    private IncidentRepository incidentRepository;

//    @Autowired
//    private ReactiveMongoOperations operations;


    private final int numberOfIncidents = 5;

    //---------------------------------------------------------------------------//

    @Before
    public void beforeEachTest(){
//        mvc = MockMvcBuilders.webAppContextSetup(wac).addFilters(springSecurityFilterChain).build();
//        MockHttpSession mockSession = new MockHttpSession(webAppContext.getServletContext(), UUID.randomUUID().toString());
    }

    //---------------------------------------------------------------------------//

    /*
    @MockBean
    private UserVehicleService userVehicleService;

    @Test
    public void getVehicleShouldReturnMakeAndModel() {
        given(this.userVehicleService.getVehicleDetails("sboot"))
            .willReturn(new VehicleDetails("Honda", "Civic"));

        this.mvc.perform(get("/sboot/vehicle")
            .accept(MediaType.TEXT_PLAIN))
            .andExpect(status().isOk())
            .andExpect(content().string("Honda Civic"));
    }
    */

    //-----------------------------------------------------------------------//

    @Test
    public void noops(){
//        mvc.perform(get("/").session(mockSession)).perform();
    }

    @Test
//    @WithUserDetails("user1@example.com")
    public void test_GET_All_incident() throws Exception {
//        mvc
//                .perform(get("/incidents"))
//                .andExpect(status().isOk())




//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBodyList(Incident.class);
        ;
    }


//    @Test
//    @WithUserDetails("user1@example.com")
    public void test_User_Event_by_User() throws Exception {
        mvc
                .perform(get("/events/my?userId=0"))
                .andExpect(status().isOk());
    }

    /*
    @Test
    @WithUserDetails("admin1@example.com")
    public void test_Admin_Event_by_User() throws Exception {
        mvc
                .perform(get("/events/my?userId=1"))
                .andExpect(status().is5xxServerError())
                .andExpect(view().name("error"))
                .andExpect(content()
                        .string(
                                containsString("Exception during execution of Spring Security application!")
                        )
                ).andExpect(content()
                        .string(
                                containsString("Access is denied")
                        )
                );
    }

    @Test
    public void test_AllEvents_USER() throws Exception {
        mvc
                .perform(get("/events/"))
                .andExpect(status().isForbidden());
    }*/


} // The End...