package com.baselogic.pid;

import com.baselogic.pid.controller.IncidentEndpointTests;
import com.baselogic.pid.service.IncidentServiceTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
//        IncidentServiceTests.class,
//        IncidentEndpointTests.class
})
public class IncidentSuite {}
