package com.baselogic.pid.domain;

import com.baselogic.pid.domain.incident.Comment;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.domain.incident.Person;
import com.baselogic.pid.enums.Gender;
import com.google.common.reflect.ClassPath;
import javassist.*;
import lombok.extern.slf4j.Slf4j;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.meanbean.test.BeanTestException;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;


/**
 * ClassLoader code adapted from http://stackoverflow.com/a/21430849/2464657
 * https://gist.github.com/arosini/5bb709da5931d600b6b7
 * http://jqno.nl/equalsverifier/
 *
 * https://stackoverflow.com/questions/520328/can-you-find-all-classes-in-a-package-using-reflection/21430849#21430849
 */
@RunWith(SpringRunner.class)
@Slf4j
public class DomainTests {

    private static final String MODEL_PACKAGE = "com.baselogic.pid.domain";

    private BeanTester beanTester;

    @Before
    public void before() {
        beanTester = new BeanTester();
    }


    @Test
    public void noop() {}

/*

//    @Test
    public void test__EqualsContract_Incident() {
        EqualsVerifier.forClass(Incident.class)
//                .usingGetClass()
                .withRedefinedSuperclass()
                .suppress(
                        Warning.STRICT_INHERITANCE,
                        Warning.NONFINAL_FIELDS,
                        Warning.NULL_FIELDS)
                .verify();
        Incident pojo = new Incident("foobar");

        assertThat(pojo).isNotNull();

        assertThat(pojo.getId()).isEqualTo("foobar");
    }

//    @Test
    public void test__EqualsContract_Person() {
        EqualsVerifier.forClass(Person.class)
                .withRedefinedSuperclass()
                .suppress(
                        Warning.STRICT_INHERITANCE,
                        Warning.NONFINAL_FIELDS,
                        Warning.NULL_FIELDS)
                .verify();
        Person pojo = new Person();
        pojo.setId("foobar");

        assertThat(pojo).isNotNull();

        assertThat(pojo.getId()).isEqualTo("foobar");
//        assertThat(pojo.isNew()).isEqualTo(true);

//        pojo.setPersisted(true);
//        assertThat(pojo.isNew()).isEqualTo(false);

        assertThat(pojo.isMale()).isEqualTo(true);

        pojo.setGender(Gender.FEMALE);
        assertThat(pojo.isMale()).isEqualTo(false);
    }

*/

//    @Test
    public void test__EqualsContract_Comment() {
        EqualsVerifier.forClass(Comment.class)
                .withRedefinedSuperclass()
                .suppress(
                        Warning.STRICT_INHERITANCE,
                        Warning.NONFINAL_FIELDS,
                        Warning.NULL_FIELDS)
                .verify();
        Comment pojo = new Comment();
        pojo.setId("foobar");

        assertThat(pojo).isNotNull();

        assertThat(pojo.getId()).isEqualTo("foobar");
//        assertThat(pojo.isNew()).isEqualTo(true);

//        pojo.setPersisted(true);
//        assertThat(pojo.isNew()).isEqualTo(false);
    }

//    @Test
    public void test__EqualsContract_User() {
        EqualsVerifier.forClass(User.class)
                .withRedefinedSuperclass()
                .suppress(
                        Warning.STRICT_INHERITANCE,
                        Warning.NONFINAL_FIELDS,
                        Warning.NULL_FIELDS)
                .verify();
    }


    /*
    https://www.programcreek.com/java-api-examples/?api=nl.jqno.equalsverifier.EqualsVerifier

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(PathIds.class)
                .usingGetClass()
                .suppress(Warning.NULL_FIELDS)
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }*/

    //---------------------------------------------------------------------------//
//    @Test
    public void testAbstractModels() throws IllegalArgumentException, BeanTestException, InstantiationException,
            IllegalAccessException, IOException, AssertionError, NotFoundException, CannotCompileException {
        log.info("----------------------------------------------------");
        log.info("*** testAbstractModels()...");
        // Loop through classes in the model package
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClassesRecursive(MODEL_PACKAGE)) {
            final Class<?> clazz = info.load();

            // Only test abstract classes
            if (Modifier.isAbstract(clazz.getModifiers())) {
                // Test #equals and #hashCode
                EqualsVerifier.forClass(clazz).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
            }
        }
    }

//    @Test
    public void testConcreteModels()
            throws IOException, InstantiationException, IllegalAccessException, NotFoundException, CannotCompileException {
        log.info("----------------------------------------------------");
        log.info("*** testConcreteModels()...");
        // Loop through classes in the model package
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClassesRecursive(MODEL_PACKAGE)) {
            final Class<?> clazz = info.load();

            // Skip abstract classes, interfaces and this class.
            int modifiers = clazz.getModifiers();
            if (Modifier.isAbstract(modifiers) || Modifier.isInterface(modifiers) || clazz.equals(this.getClass())) {
                continue;
            }

            // Test getters, setters and #toString
            beanTester.testBean(clazz);

            // Test #equals and #hashCode
            EqualsVerifier.forClass(clazz)
                    .withRedefinedSuperclass()
                    .suppress(
                            Warning.STRICT_INHERITANCE,
                            Warning.NONFINAL_FIELDS,
                            Warning.NULL_FIELDS)
                    .verify();

//            EqualsVerifier.forClass(clazz).withRedefinedSuperclass()
//                    .suppress(
//                            Warning.STRICT_INHERITANCE,
//                            Warning.INHERITED_DIRECTLY_FROM_OBJECT,
//                            Warning.NONFINAL_FIELDS
//                    )
//                    .verify();
            // Warning.STRICT_INHERITANCE
            // Warning.STRICT_INHERITANCE,
            // Warning.NONFINAL_FIELDS

            // Verify not equals with subclass (for code coverage with Lombok)
            log.debug("******* clazz.getName(): {}", clazz.getName());
            Assert.assertFalse(clazz.newInstance().equals(createSubClassInstance(clazz.getName())));
            clazz.newInstance().hashCode();
        }
    }

    // Adapted from http://stackoverflow.com/questions/17259421/java-creating-a-subclass-dynamically
    private Object createSubClassInstance(String superClassName)
            throws NotFoundException, CannotCompileException, InstantiationException, IllegalAccessException {
        ClassPool pool = ClassPool.getDefault();

        // Create the class.
        CtClass subClass = pool.makeClass(superClassName + "Extended");
        final CtClass superClass = pool.get(superClassName);
        subClass.setSuperclass(superClass);
        subClass.setModifiers(Modifier.PUBLIC);

        // Add a constructor which will call super( ... );
        CtClass[] params = new CtClass[]{};
        final CtConstructor ctor = CtNewConstructor.make(params, null, CtNewConstructor.PASS_PARAMS, null, null, subClass);
        subClass.addConstructor(ctor);

        // Add a canEquals method
        final CtMethod ctmethod = CtNewMethod
                .make("public boolean canEqual(Object o) { return o instanceof " + superClassName + "Extended; }", subClass);
        subClass.addMethod(ctmethod);

        return subClass.toClass().newInstance();
    }

} // The End...
