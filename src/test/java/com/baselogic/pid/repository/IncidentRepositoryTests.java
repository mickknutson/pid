package com.baselogic.pid.repository;

import com.baselogic.pid.domain.incident.Incident;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

// Assert-J
// --> assertThat(result.size()).isGreaterThan(0);
// http://joel-costigliola.github.io/assertj/index.html
import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assert.*;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@DataMongoTest
@Slf4j
public class IncidentRepositoryTests {

    @Autowired
    private IncidentRepository incidentRepository;

    @Autowired
    private MongoOperations operations;

    private final int numberOfIncidents = 25;

    //--- Test methods ----------------------------------------------------//

    @Before
    public void beforeEachTest(){
        // NOTE Add many notes....
        log.info("****************************************************");
        log.info("*** beforeEachTest...");
/*

//        incidentRepository.deleteAll().then().block();
        operations.collectionExists(Incident.class)
                .flatMap(exists -> exists ? operations.dropCollection(Incident.class) : Mono.just(exists))
                .flatMap(o -> operations.createCollection(Incident.class, CollectionOptions.empty()))
                .then()
                .block();

        // NOTE What is this for????
//        IncidentFixture.initIncident(incidentRepository, numberOfIncidents);
        log.info("incidentRepository.count(): {}", incidentRepository.count());

        List<Incident> incidents = incidentRepository.findAll();

        incidents.forEach(System.out::println);
*/

        log.info("----------------------------------------------------");

    }


    @Test
    public void test__Create_Incident_records() {
        log.info("----------------------------------------------------");
        log.info("*** running...");

        List<Incident> result = incidentRepository.findAll();






        /*Mono<List<Incident>> result = fluxResult.collectList();
        List<Incident> list = result.block();
        assertThat(IterableUtils.size(list), is(numberOfIncidents));

        log.info("----------------------------------------------------");
        list.forEach(System.out::println);*/

        log.info("----------------------------------------------------");

    }

    /*@Test
    public void test__create_and_update_record() {
        log.info("*** test__create_and_update_record...");

        Incident incident = IncidentFixture.incident(454);
        incident.setLocation(HIGH_NOSE);
        incidentRepository.save(incident);
        log.info("incident saved: {}", incident);


        Optional<Incident> optResult = incidentRepository.findById("NameOfJumper454");
        Incident result = optResult.get();
        Date created = result.getCreatedDate();
        Date lastModified = result.getLastModifiedDate();

        assertThat(result.getCreatedDate(), is(notNullValue()));
//        assertThat(result.isPersisted(), is(true));

        result.setDescription("some description");
        result.setPersisted(true);

        incidentRepository.save(result);

        optResult = incidentRepository.findById("NameOfJumper454");
        result = optResult.get();

        assertThat(result.getCreatedDate().compareTo(created), is(0));
        assertThat(result.getLastModifiedDate().after(lastModified), is(true));

        log.info("incident saved: {}", incident);
        log.info("incident updated: {}", result);
        log.info("----------------------------------------------------");

    }*/

} // The End...
