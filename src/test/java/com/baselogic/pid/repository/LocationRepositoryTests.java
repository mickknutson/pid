package com.baselogic.pid.repository;

import com.baselogic.pid.domain.incident.Location;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.baselogic.pid.changelogs.DatabaseSeeder.TWIN_FALLS;

// Assert-J
// --> assertThat(result.size()).isGreaterThan(0);
// http://joel-costigliola.github.io/assertj/index.html
import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assert.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;


// Hamcrest:
// --> assertThat(result.size()).is(greaterThan(0));

//import static org.junit.Assert.*;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@DataMongoTest
@Slf4j
public class LocationRepositoryTests {

    @Autowired
    private LocationRepository locationRepository;

    //--- Test methods ----------------------------------------------------//

    @Before
    public void beforeEachTest(){
    }

    @Test
    public void test__create_new_location() {
        log.info("----------------------------------------------------");
        log.info("*** test__Create_Location_records...");

        List<Location> result = locationRepository.findAll();
        assertThat(locationRepository.count()).isGreaterThan(0);

        locationRepository.save(TWIN_FALLS);
        assertThat(locationRepository.count()).isGreaterThan(1);


        // https://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions-third-party
        // http://blog.codeleak.pl/2017/11/junit-5-meets-assertj.html
        // AssertJ Soft assertions:
        assertSoftly(
                softAssertions -> {
//                    softAssertions.assertThat(result).contains(TWIN_FALLS);
//                    softAssertions.assertThat(result).contains(TWIN_FALLS);
//                    softAssertions.assertThat(result).contains(TWIN_FALLS);
                }
        );

        log.info("----------------------------------------------------");
        result.forEach(System.out::println);

        log.info("----------------------------------------------------");

    }

    @Test
    public void test__update_location() {
        log.info("----------------------------------------------------");
        log.info("*** test__Create_Location_records...");

        List<Location> result = locationRepository.findAll();
        assertThat(result.size()).isGreaterThan(0);

        locationRepository.save(TWIN_FALLS);
        assertThat(locationRepository.count()).isGreaterThan(1);

        log.info("----------------------------------------------------");
        result.forEach(System.out::println);

        log.info("----------------------------------------------------");

    }


} // The End...
