package com.baselogic.pid.repository;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.domain.incident.Comment;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.enums.Roles;
import com.baselogic.pid.service.UserService;
import com.baselogic.pid.test.fixture.IncidentFixture;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;


@RunWith(SpringRunner.class)
@SpringBootTest//(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DataMongoTest
@Slf4j
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReactiveMongoOperations operations;

    private final int numberOfUsers = 25;

    //--- Test methods ----------------------------------------------------//

//    @Before
//    public void setUp() {

    // FIXME: Figure out how to seed the MongoDB
    @Before
    public void beforeEachTest(){
        log.info("****************************************************");
        log.info("*** beforeEachTest...");

//        userRepository.deleteAll().then().block();
        operations.collectionExists(User.class)
                .flatMap(exists -> exists ? operations.dropCollection(User.class) : Mono.just(exists))
                .flatMap(o -> operations.createCollection(User.class, CollectionOptions.empty()))
                .then()
                .block();


        IncidentFixture.initUsers(userRepository, numberOfUsers);

        User mick = new User("454", "mickknutson", "password");
        mick.addRole(Roles.ADMIN.name());
        mick.addRole(Roles.EDITOR.name());
        userRepository.save(mick);

        log.info("userRepository.count(): {}", userRepository.count());

        List<User> users = userRepository.findAll();

        users.forEach(System.out::println);



//        userRepository
//                .save(Flux.just(new User("T Shirt", "Spring Guru printed T Shirt", new BigDecimal(125), "tshirt1.png"),
//                        new User("T Shirt", "Spring Guru plain T Shirt", new BigDecimal(115), "tshirt2.png"),
//                        new User("Mug", "Spring Guru printed Mug", new BigDecimal(39), "mug1.png"),
//                        new User("Cap", "Spring Guru printed Cap", new BigDecimal(66), "cap1.png")))
//
//                .then()
//                .block();

        log.info("----------------------------------------------------");
    }
    @After
    public void afterEachTest(){
        log.info("****************************************************");
        log.info("*** afterEachTest...");

        List<User> users = userRepository.findAll();
        users.forEach(System.out::println);
        log.info("----------------------------------------------------");
    }







    @Test
    public void test__AddMultipleUsersToDB() {
        log.info("----------------------------------------------------");
        log.info("*** running...");

        List<User> result = userRepository.findAll();

        log.info("userRepository.count(): {}", userRepository.count());
        log.info("----------------------------------------------------");
        result.forEach(System.out::println);
//FIXME:        assertThat(IterableUtils.size(result), is(++numberOfUsers));

//        result.forEach((incident -> {
//            assertThat(incident.getComments().size(), is(3));
//            assertThat(incident.getReferenceUrls().size(), is(1));
//        }));
    }

    @Test
    public void test__findByUsername() {

        User result = userRepository.findByUsername("mickknutson");

        assertThat(result.getUsername(), is("mickknutson"));
    }
} // The End...
