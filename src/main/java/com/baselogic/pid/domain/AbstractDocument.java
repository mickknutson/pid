package com.baselogic.pid.domain;

import com.baselogic.pid.domain.incident.Comment;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;

import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@Data
public abstract class AbstractDocument
        implements Serializable
{

    @Indexed
    @Id
    protected Long id;

    @Version
    protected Long version;

    @CreatedDate
    private Instant created;

    @LastModifiedDate
    private Instant updated;


    //-----------------------------------------------------------------------//
//    private Collection<Comment> comments; // = new ArrayList<Comment>();

//    public void addComment(Comment comment){
//        comments.add(comment);
//    }


    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
