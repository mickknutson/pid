package com.baselogic.pid.domain.incident;

import com.mongodb.DBObject;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

/**
 * Image
 */
//TODO: Not sure if this is its own document:
@Data
public class Image implements Serializable {

    String creator;
    String text;
    Instant timestamp;

    /* FIXME: Complete:
    DBObject image = new BasicDBObject("_id", 1);
    doc.put("fileName", resourceName);
    doc.put("size", imageBytes.length);
    doc.put("data", imageBytes);
    */
    DBObject image;
    String fileName;
    String size;
    String data;


    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
