package com.baselogic.pid.domain.incident;

import com.baselogic.pid.enums.Activity;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document//(collection="incident")
@Data
//@Builder

//@AllArgsConstructor
public class Incident //implements Persistable<String>
{

    @Id
    private String id;
    //@Indexed
    private Person person;

//    @Indexed(name = "date_of_incident", direction = IndexDirection.DESCENDING)
    private LocalDateTime dateOfIncident;

    @Version
    private Long version;

    @CreatedDate
    public LocalDateTime createDate;
    @LastModifiedDate
    public LocalDateTime lastModifiedDate;


    @DBRef(db="location")
    private Location location;

    private Activity activity;
    private String tagLine;
    private String description;

    private List<Malfunction> malfunctions = new ArrayList<>(5);
    private List<Factor> factors = new ArrayList<>(5);
    private List<Equipment> equipment = new ArrayList<>(5);
    private List<Comment> comments = new ArrayList<>(5);

    // Incident Images
    private List<Image> images = new ArrayList<>(5);
    private List<URL> referenceUrls = new ArrayList<>(5);



    //-----------------------------------------------------------------------//
    // Constructors:

    public Incident(final Person person){
        this.person = person;
    }

    //-----------------------------------------------------------------------//
    // Misc:


    //-----------------------------------------------------------------------//
    // Common methods

    public void addComment(final Comment comment){
        comments.add(comment);
    }

    // NOTE: setter/getter Handled by Lombok

} // The End...
