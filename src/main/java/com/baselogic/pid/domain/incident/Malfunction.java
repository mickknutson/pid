package com.baselogic.pid.domain.incident;

import com.baselogic.pid.domain.AbstractDocument;
import lombok.Data;

import java.io.Serializable;

/**
 * Malfunction Types
 * This is the issue(s) that was the cause of the incident.
 * i.e. Low-pull, off-heading, etc..
 */
@Data
public class Malfunction
{

    String name;
    String description;


    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
