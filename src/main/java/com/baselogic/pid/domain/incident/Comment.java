package com.baselogic.pid.domain.incident;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import java.util.Date;

/**
 * Comments
 */
@Data
public class Comment {

    @Id
    String id;

    @Version
    Long version;
    @CreatedDate
    Date createdDate;
    @LastModifiedDate
    Date lastModifiedDate;

    String owningClass;

    // person who added the comment
    String name;
    String comment;

    Boolean hidden;


    //-----------------------------------------------------------------------//
    // Misc:


    //-----------------------------------------------------------------------//
    // Common methods

    // NOTE: setter/getter Handled by Lombok

} // The End...
