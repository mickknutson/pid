package com.baselogic.pid.domain.incident;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Nationality
 */
@Data
@NoArgsConstructor
public class Nationality implements Serializable {

    String iso;
    String name;
    String description;


    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
