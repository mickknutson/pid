package com.baselogic.pid.domain.incident;

import com.baselogic.pid.enums.ObjectType;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Incident Location
 */
@Document(collection="location")
@Data
public class Location {

    @Id
    private String id;

    @Indexed
    private String name;

    private ObjectType objectType;
    private String alternateName;
    private String alternateExit;
    private String city;
    private String state;
    private String country;
    private String countryCode;
    private String description;

//    @Version
//    private Long version;

    @CreatedDate
    public LocalDateTime createDate;
    @LastModifiedDate
    public LocalDateTime lastModifiedDate;


    // Notes can be private
    private List<Comment> comments = new ArrayList<>(5);


    //-----------------------------------------------------------------------//
    // Constructors:
    public Location(){}
    public Location(final String name){
        this.name = name;
    }

    //-----------------------------------------------------------------------//
    // Misc:


    //-----------------------------------------------------------------------//
    // Common methods

    public void addComment(final Comment comment){
        comments.add(comment);
    }

    // NOTE: setter/getter Handled by Lombok

} // The End...
