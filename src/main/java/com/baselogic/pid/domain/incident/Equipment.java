package com.baselogic.pid.domain.incident;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Equipment Used
 */
@Data
public class Equipment implements Serializable {

    @NotBlank
    String suit;
    String container;
    String parachute;
    String deploymentMethod;

    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
