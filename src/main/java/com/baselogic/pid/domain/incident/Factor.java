package com.baselogic.pid.domain.incident;

import lombok.Data;

/**
 * Incident Factors
 * These are the factor(s) that was the caused of the incident.
 * i.e. Fatigue, dehydration, darkness, cold, lack of experience, etc.
 * ...
 * Slipping, falling, wet (water), ice, snow.
 */
@Data
public class Factor {

    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
