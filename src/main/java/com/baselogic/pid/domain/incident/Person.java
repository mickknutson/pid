package com.baselogic.pid.domain.incident;


import com.baselogic.pid.enums.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
//@NoArgsConstructor
public class Person
        implements Serializable {

//    @Id
//    private String id;
    private String nickName;

    @NotNull
    private String firstName;
    private String middleName;
    @NotNull
    private String lastName;
    private String email;

    private int age;
    private LocalDate dob;

    private int height;
    private int weight;
    private Gender gender = Gender.MALE;


    // Person Images
    private List<Image> images = new ArrayList<>(5);

    private List<URL> referenceUrls = new ArrayList<>(5);

    private Nationality nationality;
    private int numberOfSkydives;
    private int numberOfBaseJumps;
    private int hoursOfParagliding;

    //-----------------------------------------------------------------------//
    // Constructor:
    public Person(final String firstName,
                  final String middleName,
                  final String lastName,
                  final String nickName){
        this.setFirstName(firstName);
        this.setMiddleName(middleName);
        this.setLastName(lastName);
        this.setNickName(nickName);
    }

    //-----------------------------------------------------------------------//
    // Misc:
    public Boolean isMale(){
        return getGender().equals(Gender.MALE);
    }


    //-----------------------------------------------------------------------//
    // Common methods
    // NOTE: setter/getter Handled by Lombok

} // The End...
