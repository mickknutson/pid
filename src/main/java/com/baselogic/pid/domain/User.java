package com.baselogic.pid.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection="user")
@Data
public class User {

    @Id
    private String id;
    private String username;
    private String password;

    private List<String> roles = new ArrayList<>(5);

    public User() {}

    @PersistenceConstructor
    public User(String id, String username, String password) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public void addRole(String role) {
        this.roles.add(role);
    }

    //--- Common methods ----------------------------------------------------//
    // NOTE: Handled by Lombok

} // The End...
