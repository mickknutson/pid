package com.baselogic.pid.service;

import com.baselogic.pid.domain.User;
import reactor.core.publisher.Flux;

import java.util.List;

public interface UserService {

    User findByUsername(String username);

    void save(User user);

    List<User> findAll();

    void deleteAll();

} // The End...
