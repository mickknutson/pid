package com.baselogic.pid.service;

import com.baselogic.pid.domain.incident.Comment;
import com.baselogic.pid.domain.incident.Incident;

import java.util.List;

public interface LocationService {

    Incident insert(Incident incident);
    Incident save(Incident incident);
    Comment addComment(Comment comment);

    Incident find(String id);
    List<Incident> findAll();

    void deleteAll();

} // The End...
