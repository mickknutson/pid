package com.baselogic.pid.service;

import com.baselogic.pid.domain.User;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class AuthenticatedUser //implements UserDetails
{

    private static final long serialVersionUID = 2059202961588104658L;

    private User user; // user is my own model, not of spring-framework

    public AuthenticatedUser(final User user) {
        this.user = user;
    }

    /*@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> auths = new java.util.ArrayList<>();
        // FIXME: user == null
        for(String role: user.getRoles()){
            auths.add(new SimpleGrantedAuthority(role));
        }
        return auths;
    }

    @Override
    public String getPassword(){
        return this.user.getPassword();
    }*/

    /**
     * Returns the username used to authenticate the user. Cannot return <code>null</code>
     * .
     *
     * @return the username (never <code>null</code>)
     */
    /*@Override
    public String getUsername(){
        return this.user.getUsername();
    }*/

    /**
     * Indicates whether the user's account has expired. An expired account cannot be
     * authenticated.
     *
     * @return <code>true</code> if the user's account is valid (ie non-expired),
     * <code>false</code> if no longer valid (ie expired)
     */
    /*@Override
    public boolean isAccountNonExpired(){
        return true;
    }*/

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be
     * authenticated.
     *
     * @return <code>true</code> if the user is not locked, <code>false</code> otherwise
     */
    /*@Override
    public boolean isAccountNonLocked(){
        return true;
    }*/

    /**
     * Indicates whether the user's credentials (password) has expired. Expired
     * credentials prevent authentication.
     *
     * @return <code>true</code> if the user's credentials are valid (ie non-expired),
     * <code>false</code> if no longer valid (ie expired)
     */
    /*@Override
    public boolean isCredentialsNonExpired(){
        return true;
    }*/

    /**
     * Indicates whether the user is enabled or disabled. A disabled user cannot be
     * authenticated.
     *
     * @return <code>true</code> if the user is enabled, <code>false</code> otherwise
     */
    /*@Override
    public boolean isEnabled(){
        return true;
    }*/

} // The End...
