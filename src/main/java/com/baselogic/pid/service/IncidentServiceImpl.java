package com.baselogic.pid.service;

import com.baselogic.pid.domain.incident.Comment;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.repository.CommentRepository;
import com.baselogic.pid.repository.IncidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Component
public class IncidentServiceImpl implements IncidentService {


    @Autowired
    private IncidentRepository incidentRepository;

    @Autowired
    private CommentRepository commentRepository;


//    @PostConstruct//

    @PreDestroy
    public void doBeforeDestroy(){

    }


    // Constructor Injection
    public IncidentServiceImpl(IncidentRepository incidentRepository){
        this.incidentRepository = incidentRepository;
    }


    // Setter Injection
    public void setIncidentRepository(IncidentRepository repo){
        this.incidentRepository = repo;
    }








    @Override
    public Incident insert(Incident incident) {
//        incident.getComments().forEach((comment) -> {commentRepository.insert(comment);});
//        return incidentRepository.insert(incident);
        return incidentRepository.insert(incident);
    }

    @Override
    public Incident save(Incident incident) {
        return incidentRepository.save(incident);
//        commentRepository.save(incident.getComments());
    }

    @Override
    public Comment addComment(Comment comment) {
        return commentRepository.insert(comment);
    }


    @Override
    public Incident find(String id) {
        return incidentRepository.findById(id).get();
    }

    @Override
    public List<Incident> findAll() {
        List<Incident> fluxResult = incidentRepository.findAll();
        return fluxResult;
    }

    @Override
    public void deleteAll() {
        incidentRepository.deleteAll();
        commentRepository.deleteAll();
    }


} // The End...
