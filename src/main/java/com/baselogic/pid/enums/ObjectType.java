package com.baselogic.pid.enums;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum ObjectType {

    BUILDING,
    ANTENNA,
    SPAN,
    EARTH,
    OTHER;

    public static ObjectType instance(String text) {
        if (text != null) {
            for (ObjectType entry : ObjectType.values()) {
                if (text.equalsIgnoreCase(entry.name())) {
                    return entry;
                }
            }
        }
        log.warn("Invalid ObjectType: " + text);
        return null;
    }

}
