package com.baselogic.pid.enums;

public enum Activity {

    BASE,
    SKYDIVE,
    PARAGLIDE
}
