package com.baselogic.pid.enums;

public enum Gender {
    MALE,
    FEMALE
}
