package com.baselogic.pid.enums;

/**
 * Not sure how to relate this to a possible
 * message bundle.
 */
public enum Malfunction {

    NO_PULL,
    LOW_PULL,
    OFF_HEADING
}
