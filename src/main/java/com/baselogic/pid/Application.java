package com.baselogic.pid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

//import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
//import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;
//import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
//import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}



    /**
     * DEFAULT: all public repository interfaces but considers settings on annotations
     * ALL: all repositories independently of type visibility and annotations
     * ANNOTATION: only annotated repositories unless they are set to false
     * VISIBILITY: only public annotated repositories
     *
     * @return
     */
    /*@Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {

        return new RepositoryRestConfigurerAdapter() {

            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.setRepositoryDetectionStrategy(
//                        RepositoryDetectionStrategy.RepositoryDetectionStrategies.DEFAULT);
                        RepositoryDetectionStrategy.RepositoryDetectionStrategies.ALL);
//                        RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);
//                        RepositoryDetectionStrategy.RepositoryDetectionStrategies.VISIBILITY);
            }
        };
    }*/

} // The End...
