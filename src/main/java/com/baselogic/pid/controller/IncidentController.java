package com.baselogic.pid.controller;

//  <bean id="com.baselogic.pid.controller.IncidentController" >


import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.repository.IncidentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/incidents")
@Slf4j
public class IncidentController {

    @Autowired
    private IncidentRepository incidentRepository;

    @GetMapping("/echo/{echo}")
    public String echo(@PathVariable String echo) {
        log.info("*** echo: {}", echo);

//        return "echo:" + echo;
        return String.format("Echoing the following: (%s)", echo);
    }



    @GetMapping//("/incidents")
    public Flux<Incident> findAllIncidents() {
        log.info("*** findAllIncidents() *************************************************");
        /*Flux<Incident> incidents = incidentRepository.findAll();

        log.info("*** incidents: ");
        incidents.doOnEach(System.out::println);

        return incidents;*/
        return null;
    }

    @PostMapping//("/incidents")
    public Mono<Incident> newIncident(final @Valid @RequestBody Incident incident) {
        log.info("*** new Incident(): {} *************************************************", incident);
        /*Mono<Incident> result = incidentRepository.save(incident);

        log.info("*** newIncident: {}", result);

        log.info("----------");
        return result;*/
        return null;
    }

    @PutMapping("/incidents2")
    public Mono<Incident> updateIncident2(final @Valid @RequestBody Incident incident) {
        /*Mono<Incident> result = incidentRepository.save(incident);

        log.info("*** result: {}", result);
        return result;*/
        return null;
    }


    /*@PutMapping//("/incidents/")
    public Mono<ResponseEntity<Incident>> updateIncident(final @Valid @RequestBody Incident incident) {
        return incidentRepository.findById(incident.getPerson())
                .flatMap(existingIncident -> {
//                    existingTweet.setText(incident.getText());
                    return incidentRepository.save(incident);
                })
                .map(updatedIncident -> new ResponseEntity<>(updatedIncident, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }*/





    /*
    @Autowired
    private TweetRepository tweetRepository;

    @GetMapping("/tweets")
    public Flux<Tweet> getAllTweets() {
        return tweetRepository.findAll();
    }

    @PostMapping("/tweets")
    public Mono<Tweet> createTweets(@Valid @RequestBody Tweet tweet) {
        return tweetRepository.save(tweet);
    }

    @GetMapping("/tweets/{id}")
    public Mono<ResponseEntity<Tweet>> getTweetById(@PathVariable(value = "id") String tweetId) {
        return tweetRepository.findById(tweetId)
                .map(savedTweet -> ResponseEntity.ok(savedTweet))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/tweets/{id}")
    public Mono<ResponseEntity<Tweet>> updateTweet(@PathVariable(value = "id") String tweetId,
                                                   @Valid @RequestBody Tweet tweet) {
        return tweetRepository.findById(tweetId)
                .flatMap(existingTweet -> {
                    existingTweet.setText(tweet.getText());
                    return tweetRepository.save(existingTweet);
                })
                .map(updatedTweet -> new ResponseEntity<>(updatedTweet, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/tweets/{id}")
    public Mono<ResponseEntity<Void>> deleteTweet(@PathVariable(value = "id") String tweetId) {

        return tweetRepository.findById(tweetId)
                .flatMap(existingTweet ->
                        tweetRepository.delete(existingTweet)
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                )
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Tweets are Sent to the client as Server Sent Events
    @GetMapping(value = "/stream/tweets", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Tweet> streamAllTweets() {
        return tweetRepository.findAll();
    }
     */

} // The End...
