package com.baselogic.pid.configuration;

import com.github.mongobee.Mongobee;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration

@EnableMongoRepositories(basePackages="com.baselogic.pid.repository")
@EnableMongoAuditing
public class MongoConfig
        extends AbstractMongoConfiguration {

    private final Environment environment;

    public MongoConfig(Environment environment) {
        this.environment = environment;
    }


    @Value("mongodb://localhost:27017")
    private String uri;

    @Override
    public MongoClient mongoClient() {
        return new MongoClient(new MongoClientURI(uri));
    }

    @Override
    protected String getDatabaseName() {
        return "pid";
    }

    @Bean(name = "mongobee")
    public Mongobee mongobee() throws Exception {
        Mongobee runner = new Mongobee(uri);
        runner.setDbName(getDatabaseName());
        runner.setChangeLogsScanPackage("com.baselogic.pid.changelogs");
        runner.setMongoTemplate(mongoTemplate());
        return runner;
    }



// https://github.com/flapdoodle-oss
//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    protected Fadoodle createMondoDb() {
//        return new Flapdoodle().init();
//    }

} // The End...
