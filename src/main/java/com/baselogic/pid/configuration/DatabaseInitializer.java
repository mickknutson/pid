package com.baselogic.pid.configuration;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.domain.incident.Comment;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.domain.incident.Location;
import com.baselogic.pid.enums.ObjectType;
import com.baselogic.pid.enums.Roles;
import com.baselogic.pid.repository.CommentRepository;
import com.baselogic.pid.repository.IncidentRepository;
import com.baselogic.pid.repository.LocationRepository;
import com.baselogic.pid.repository.UserRepository;
import com.baselogic.pid.service.IncidentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * https://github.com/mongobee/mongobee
 */
//@Component
@Slf4j
public class DatabaseInitializer {

    @Autowired
    private ReactiveMongoOperations operations;
//    private MongoOperations operations;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IncidentRepository incidentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CommentRepository commentRepository;


    //---------------------------------------------------------------------------//

//    public DatabaseInitializer() {
//        this.posts = posts;
//    }
//
//    @Override
//    public void run(String[] args) {
//        log.info("start data initialization  ...");
//        this.posts
//                .deleteAll()
//                .thenMany(
//                        Flux
//                                .just("Post one", "Post two")
//                                .flatMap(
//                                        title -> this.posts.save(Post.builder().title(title).content("content of " + title).build())
//                                )
//                )
//                .log()
//                .subscribe(
//                        null,
//                        null,
//                        () -> log.info("done initialization...")
//                );
//
//    }

//    @EventListener
    public void seed(final ContextRefreshedEvent event) {
        seed();
    }

//    @PostConstruct
    public void seed() {
        log.info("****************************************************");
        log.info("*** running...DatabaseInitializer");

        // Incident details:
        seedEquipment();
        seedFactors();
        seedImages();
//        seedLocations();
        seedMalfunctions();
        seedNationality();
        seedPersons();

        // Users & Persons are almost last
        seedUsers();

        // Incidents must be last:
        seedIncidents();

        log.info("*** END...*************************************************");
    }


    //---------------------------------------------------------------------------//






    private void seedUsers() {
        log.info("****************************************************");
        log.info("*** running...seedUsers");
        // This is the current master user.
        addUser("admin", "admin", Roles.ADMIN.name());

        // This is the current master user.
        addUser("mickknutson", "password", Roles.USER.name(), Roles.ADMIN.name());

    }

    private void addUser(String username, String password, String ... roles) {
        /*if (!userExists(username)) {
            log.info("Adding user {} (username = {}, password = {})", username, password);
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);

            if (roles != null && roles.length > 0) {
                for(String role: roles){
                    log.info("--> Add Role ({}) to User ({}). ", role, username);
                    user.addRole(role);
                }
            }
//            userRepository.save(user).then().subscribe();
            userRepository.save(user).then().block();

        }*/
    }

    private boolean userExists(String username) {
        log.info("******** userExists: {} ************", username);
        /*Mono<User> user =  userRepository.findByUsername(username);
        User result = user.block();
        log.info("******** existsUserByUsername: {} ", result);
        return (result == null);*/
        return true;
    }


    //---------------------------------------------------------------------------//


    private void seedEquipment() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private void seedFactors() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private void seedImages() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private void seedIncidents(){
        log.info("****************************************************");
        log.info("*** running...seedIncidents");

//        List<Incident> seeds = new ArrayList<>(10);
//
//        for(int i = 0; i < 10; i++) {
//            final Incident incident = IncidentFixture.incident(i);
//            incident.setId(null);
//            incident.setLocation(HIGH_NOSE);
//
//            List<Comment> comments = IncidentFixture.comments(incident.getTagLine());
//
//            comments.forEach((comment) -> {
//                Comment c = incidentService.addComment(comment);
//                incident.addComment(c);
//            });
//
//            seeds.add(incident);
//
//        }
//        return seeds;

    }


    //---------------------------------------------------------------------------//
    private void seedMalfunctions() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private void seedNationality() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private void seedPersons() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

} // The End...
