package com.baselogic.pid.configuration;

import com.baselogic.pid.service.UserAuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

/*
The SecurityConfig will:
=========================
    Require authentication to every URL in your application
    Generate a login form for you
    Allow the user with the Username user and the Password password to authenticate with form based authentication
    Allow the user to logout
    CSRF attack prevention
    Session Fixation protection
    Security Header integration:
    ============================
        HTTP Strict Transport Security for secure requests
        X-Content-Type-Options integration
        Cache Control (can be overridden later by your application to allow caching of your static resources)
        X-XSS-Protection integration
        X-Frame-Options integration to help prevent Click-jacking
    Integrate with the following Servlet API methods:
    =================================================
        HttpServletRequest#getRemoteUser()
        HttpServletRequest.html#getUserPrincipal()
        HttpServletRequest.html#isUserInRole(java.lang.String)
        HttpServletRequest.html#login(java.lang.String, java.lang.String)
        HttpServletRequest.html#logout()
*/


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * HTTP Security configuration
     *
     * @param http HttpSecurity configuration.
     * @throws Exception Authentication configuration exception
     * http://docs.spring.io/spring-security/site/migrate/current/3-to-4/html5/migrate-3-to-4-jc.html
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()


                .antMatchers("/**").permitAll()

//                .antMatchers("/admin/**").hasAuthority("ADMIN")
//                .antMatchers("/editor/**").hasAuthority("EDITOR")

//                .antMatchers("/admin/**").access("hasRole('ADMIN')")
//                .antMatchers("/customers/*").access("hasRole('USER') or hasRole('MANAGER')")
//                .antMatchers("/managers/*").access("hasRole('MANAGER') or hasRole('ADMIN') or hasRole('DBA')")
                .antMatchers("/", "/index").permitAll()



                .and().anonymous()
                .and().rememberMe()
                .and()


                /* Replace the following default logout, with a custom logout logic */
                .logout()
                    .logoutUrl("/j_spring_security_logout")
                    .logoutSuccessUrl("/index")


                /* Add exceptionHandling */
                .and().exceptionHandling().accessDeniedPage("/Access_Denied.html?error=true")
        ;


        http.csrf().disable();
        /* end edits */

        // Disallow multiple Sessions at the same time.
        http.sessionManagement()
                .maximumSessions(1)
                .expiredUrl("/expired.html")
                .sessionRegistry(sessionRegistry());

        log.info("***** configured  HttpSecurity");
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("password").roles("USER")
                .and()
                .withUser("user1").password("changeme").roles("USER")
                .and()
                .withUser("admin").password("admin").roles("USER", "ADMIN")
        ;
    }


    /**
     * Password encoder using BCrypt.
     *
     * @return org.springframework.security.crypto.password.PasswordEncoder
     * @see BCryptPasswordEncoder
     */
    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(4);
    }



    // ConcurrentSessionFilter support
    @Bean
    public SessionRegistry sessionRegistry () {
        return new SessionRegistryImpl();
    }

} // The End...

//---------------------------------------------------------------------------//
//---------------------------------------------------------------------------//
//---------------------------------------------------------------------------//
//---------------------------------------------------------------------------//
//---------------------------------------------------------------------------//
