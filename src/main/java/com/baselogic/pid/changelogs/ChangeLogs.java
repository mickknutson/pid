package com.baselogic.pid.changelogs;

import com.baselogic.pid.domain.User;
import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.domain.incident.Location;
import com.baselogic.pid.repository.LocationRepository;
import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;

import java.net.MalformedURLException;

/**
 * https://github.com/mongobee/mongobee
 */
@ChangeLog
@Slf4j
public class ChangeLogs {

    @ChangeSet(order = "001",
            id = "1.0.1", author = "mickknutson",
            runAlways = true)
    public void initialSeed(MongoTemplate template, Environment environment){
        log.info("****************************************************");
        log.info("*** running...initialSeed");
        template.dropCollection(Incident.class);

        template.collectionExists(Location.class);

        template.dropCollection(Location.class);
        template.dropCollection(User.class);
    }

    @ChangeSet(order = "002",
            id = "1.0.2", author = "mickknutson", runAlways = true)
    public void changelog002(final MongoTemplate template,
                             final Environment environment)
            throws MalformedURLException {
        log.info("****************************************************");
        log.info("*** running...changelog002");
        // type: org.springframework.data.mongodb.core.MongoTemplate
        // type: org.springframework.core.env.Environment
        // Spring Data integration allows using MongoTemplate and Environment in the ChangeSet

        DatabaseSeeder.seedLocations(template);
        DatabaseSeeder.seedIncidents(template);
    }


//    @ChangeSet(order = "001", id = "someChangeWithoutArgs", author = "testAuthor")
//    public void someChange1() {
//        // method without arguments can do some non-db changes
//    }
//
//    @ChangeSet(order = "002", id = "someChangeWithMongoDatabase", author = "testAuthor")
//    public void someChange2(MongoDatabase db) {
//        // type: com.mongodb.client.MongoDatabase : original MongoDB driver v. 3.x, operations allowed by driver are possible
//        // example:
//        MongoCollection<Document> mycollection = db.getCollection("mycollection");
//        Document doc = new Document("testName", "example").append("test", "1");
//        mycollection.insertOne(doc);
//    }
//
//    @ChangeSet(order = "003", id = "someChangeWithDb", author = "testAuthor")
//    public void someChange3(DB db) {
//        // This is deprecated in mongo-java-driver 3.x, use MongoDatabase instead
//        // type: com.mongodb.DB : original MongoDB driver v. 2.x, operations allowed by driver are possible
//        // example:
//        DBCollection mycollection = db.getCollection("mycollection");
//        BasicDBObject doc = new BasicDBObject().append("test", "1");
//        mycollection .insert(doc);
//    }



} // The End...