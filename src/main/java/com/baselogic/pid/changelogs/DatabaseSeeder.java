package com.baselogic.pid.changelogs;

import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.domain.incident.Location;
import com.baselogic.pid.domain.incident.Person;
import com.baselogic.pid.enums.Activity;
import com.baselogic.pid.enums.ObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

@Slf4j
public class DatabaseSeeder {


    public static void seed() {
        log.info("****************************************************");
        log.info("*** running...DatabaseInitializer");

        // Reference Documents:
//        seedLocations();

        // Embedded Documents:
        seedFactors();
        seedEquipment();
        seedImages();

        seedMalfunctions();

//        seedPersons();
        seedNationality();

        // Users & Persons are almost last
        seedUsers();

        // Incidents must be last:
        seedIncidents();

        log.info("*** END...*************************************************");
    }


    //---------------------------------------------------------------------------//
    //---------------------------------------------------------------------------//
    //---------------------------------------------------------------------------//
    //---------------------------------------------------------------------------//

    public static void seedLocations(final MongoTemplate template) {
        log.info("****************************************************");
        log.info("*** running...seedLocations");

        boolean exists = template.collectionExists(Location.class);
        template.createCollection(Location.class);
        log.info("Location Collection exists: {}", exists);

        template.save(DatabaseSeeder.HIGH_NOSE);

    }


    public static Location HIGH_NOSE = new Location(){{
        setName("High Nose");
        setObjectType(ObjectType.EARTH);
        setAlternateName("The Nose");
        setAlternateExit("Nose 3");
        setCity("Lauterbrunnen");
        setState("Bern");
        setCountry("Switzerland");
        setCountryCode("CH");
        setDescription("Typical High Nose Exit Point");
    }};


    public static Location TWIN_FALLS = new Location(){{
        setName("Twin Falls");
        setObjectType(ObjectType.SPAN);
        setAlternateName("Perrine");
        setCity("Twin Falls");
        setState("Idaho");
        setCountry("USA");
        setCountryCode("US");
        setDescription("Potato Bridge");
    }};








    private static void seedUsers() {
        log.info("****************************************************");
        log.info("*** running...seedUsers");
        // This is the current master user.
        /*addUser("admin", "admin", Roles.ADMIN.name());

        // This is the current master user.
        addUser("mickknutson", "password", Roles.USER.name(), Roles.ADMIN.name());*/
    }


    //---------------------------------------------------------------------------//
    private static void seedIncidents(){}

    public static void seedIncidents(final MongoTemplate template) throws MalformedURLException{
        log.info("****************************************************");
        log.info("*** running...seedIncidents");

        boolean exists = template.collectionExists(Incident.class);
        template.createCollection(Incident.class);
        log.info("Incident Collection exists: {}", exists);

        template.save(
                getIncident()
        );

//        List<Incident> seeds = new ArrayList<>(10);
//
//        for(int i = 0; i < 10; i++) {
//            final Incident incident = IncidentFixture.incident(i);
//            incident.setId(null);
//            incident.setLocation(HIGH_NOSE);
//
//            List<Comment> comments = IncidentFixture.comments(incident.getTagLine());
//
//            comments.forEach((comment) -> {
//                Comment c = incidentService.addComment(comment);
//                incident.addComment(c);
//            });
//
//            seeds.add(incident);
//
//        }
//        return seeds;
    }

    public static Incident getIncident() throws MalformedURLException {

        Person person = seedPerson();

        Incident incident = new Incident(person);

        incident.setDescription("Foo description");

        // FIXME: Need to ensure this is saved 1st:
        incident.setLocation(HIGH_NOSE);

        incident.setActivity(Activity.BASE);
        incident.setTagLine("tagline...");

        incident.setReferenceUrls(new ArrayList<URL>(){{add(new URL("http", "blincmagazine.com", 80, "/"));}});

        return incident;
    }


    //---------------------------------------------------------------------------//
    private static Person seedPerson() {
        log.info("****************************************************");
        log.info("*** running...seedPerson");
        Person person = new Person("Carl",
                "",
                "Boenish",
                "FooBar");
        return person;

    }





    //---------------------------------------------------------------------------//
    private static void seedEquipment() {
        log.info("****************************************************");
        log.info("*** running...seedEquipment");

    }

    //---------------------------------------------------------------------------//
    private static void seedFactors() {
        log.info("****************************************************");
        log.info("*** running...seedFactors");

    }

    //---------------------------------------------------------------------------//
    private static void seedImages() {
        log.info("****************************************************");
        log.info("*** running...seedImages");

    }


    //---------------------------------------------------------------------------//
    private static void seedMalfunctions() {
        log.info("****************************************************");
        log.info("*** running...seedMalfunctions");

    }

    //---------------------------------------------------------------------------//
    private static void seedNationality() {
        log.info("****************************************************");
        log.info("*** running...seedNationality");

    }

} // The End...
