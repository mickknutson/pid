package com.baselogic.pid.repository;

import com.baselogic.pid.domain.incident.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

//@Repository
//@RepositoryRestResource
public interface CommentRepository
        extends MongoRepository<Comment, String> {

//    @Query("{'person' : ?0}")
//    public Iterable<Comment> searchByName(String roleName);

} // The End..
