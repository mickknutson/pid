package com.baselogic.pid.repository;

import com.baselogic.pid.domain.incident.Incident;
import com.baselogic.pid.domain.incident.Location;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/**
 * Incident Repository
 */
//@Repository
@RepositoryRestResource
public interface LocationRepository
//        extends ReactiveMongoRepository<Location, String>
        extends MongoRepository<Location, String>
{

} // The End...
