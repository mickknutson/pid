package com.baselogic.pid.repository;

import com.baselogic.pid.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * spring-data
 */
//@Repository
@RepositoryRestResource
public interface UserRepository
        extends MongoRepository<User, String> {

    User findByUsername(final String username);

    Boolean existsUserByUsername(final String username);

/*
    Mono<User> findByUsername(final String username);

    Mono<Boolean> existsUserByUsername(final String username);
//    boolean existsUserByUsername(final String username);

*/
} // The End...
