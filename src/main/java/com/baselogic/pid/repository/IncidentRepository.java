package com.baselogic.pid.repository;

import com.baselogic.pid.domain.incident.Incident;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.Date;
import java.util.List;

/**
 * Incident Repository
 */
//@Repository
@RepositoryRestResource
public interface IncidentRepository
//        extends ReactiveMongoRepository<Incident, String>
        extends MongoRepository<Incident, String>
{


//    List<Incident> findIncidentBy(Date after);

//    Flux<Incident> findByLocation(final String location);

//    Mono<Incident> findByObjectType(final String objectType);

//    Flux<Product> findByName(String name);
//
//    Flux<Product> findByName(Mono<String> name);
//
//    Mono<Product> findByNameAndImageUrl(Mono<String> name, String imageUrl);
//
//    @Query("{ 'name': ?0, 'imageUrl': ?1}")
//    Mono<Product> findByNameAndImageUrl(String name, String imageUrl);

//    @Query("{'person' : ?0}")
//    public Iterable<Incident> searchByName(String roleName);


} // The End...
